UniversePay module installation

- Add service provider into .../be/config/app.php
     - UniversePay\UniversePayServiceProvider::class,
- Add config key in .env
     - UPAY_TEST_MODE=true|false
     - UPAY_SHOP_ID=12345
     - UPAY_SECRET=8e44a...371704
     - UPAY_PUBLIC_KEY=MIIBI....DAQAB
     - UPAY_CHECKOUT_URL=https://checkout.universepay.eu
     - UPAY_PAYMENT_URL=https://payment.universepay.eu
     - UPAY_RETURN_URL=https://our-dashboard-domain.com/upay/return...
       # redirect to success page (front end)
     - UPAY_FAIL_URL=https://our-dashboard-domain.com/upay/fail...
       # redirect to fail page (front end)
     - UPAY_CANCEL_URL=https://our-dashboard-domain.com/upay/cancel...
       # redirect to cancel (front end)
     - UPAY_NOTIFICATION_URL=https://our-api-domain.com/api/en-GB/v1/banking/upay/notification
       # listener of upay api responses (back end)
- Changes in composer,json
     - add in "require" section
            "spacefex/universepay": "*"
     - add in "repositories" section (you must have bitbucket repo passwd)
            {
                   "type": "vcs",
                   "url": "https://quatroit@bitbucket.org/spacefex/universepay.git"
            }
- composer install | update
