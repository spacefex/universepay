<?php

namespace UniversePay;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;

/**
 * Class UniversePayClient
 * @package UniversePay
 */
class UniversePayClient
{
    /**
     * @var Configuration $config
     */
    protected $config; 

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return object
     */
    public function getTransactionStatusByToken(string $token)
    {
        $url = $this->config->getCheckoutHost() . "/ctp/api/checkouts/$token";

        return $this->sendHttpRequest($url, [], "GET");
    }

    /**
     * @return object
     */
    public function getTransactionStatusByUID(string $uid)
    {
        $url = $this->config->getPaymentHost() . "/transactions/$uid";

        return $this->sendHttpRequest($url, [], "GET");
    }

     /**
     * @return object
     */
    public function getTransactionStatusByTrackingID(string $tracking_id)
    {
        $url = $this->config->getPaymentHost() . "/v2/transactions/tracking_id/$tracking_id";

        return $this->sendHttpRequest($url, [], "GET");
    }
 
    /**
     * @param string $applicantId
     *
     * @return object
     */
    public function createPaymentToken($params = [])
    {
        $url = $this->config->getCheckoutHost() . "/ctp/api/checkouts";

        return $this->sendHttpRequest($url, $params);
    }

    /**
     * @param Request $request
     * @param string $url
     *
     * @return ResponseInterface
     */
    private function sendHttpRequest(string $url, $params = [], string $requestType = 'POST')
    {
        if (!strlen($url)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, "[UPAY] incorrect input param in sendHttpRequest");
            // throw new \Exception("[UPAY] incorrect input param in sendHttpRequest");
        }

        $process = curl_init();
        $defaultHeaders = ['Accept: application/json', 'X-API-Version: 2'];

        // Log::info("[UPAY] $requestType request: $url");

        if ($requestType == 'POST') {
            curl_setopt($process, CURLOPT_POST, 1);
            if (!empty($params)) {
                curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($params));
            }
            curl_setopt($process, CURLOPT_HTTPHEADER,
            $defaultHeaders = array_merge(
              ['Content-type: application/json'],
              $defaultHeaders
            )
          );
        }

        curl_setopt($process, CURLOPT_HTTPHEADER, $defaultHeaders); 
        curl_setopt($process, CURLOPT_URL, $url);
        curl_setopt($process, CURLOPT_USERPWD, $this->config->getShopId() . ":" . $this->config->getSecret());
        curl_setopt($process, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, true);

        $response = curl_exec($process);
        $error = curl_error($process);
        curl_close($process);

        if ($response === false) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, "[UPAY] cURL error: " . $error);
            // throw new \Exception("[UPAY] cURL error: " . $error);
        }

        Log::info("[UPAY] Response $response");
        
        return json_decode($response);
    }
}