<?php

namespace UniversePay;

/**
 * Class Configuration
 * @package SumSub
 */
class Configuration
{
	/**
	 * API secret
	 *
	 * @var string
	 */
    protected $secret = '';

    /**
	 * API public key
	 *
	 * @var string
	 */
    protected $publicKey = '';

    /**
     * ShopId for HTTP basic authentication.
     *
     * @var string
     */
    protected $shopId = '';

    /**
     * @var string
     */
    protected $hostCheckout = '';
    protected $hostPayment = '';

    /**
     * TestMode switch (default set to false).
     *
     * @var bool
     */
    protected $testMode = false;

    /**
	 * @var string
	 */
	protected $returnURL = '';
    protected $failURL = '';
    protected $cancelURL = '';
    protected $notificationURL = '';
    protected $verificationURL = '';
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->shopId = env('UPAY_SHOP_ID');
        $this->secret = env('UPAY_SECRET');
        $this->publicKey = env('UPAY_PUBLIC_KEY');
        $this->hostCheckout = env('UPAY_CHECKOUT_URL');
        $this->hostPayment = env('UPAY_PAYMENT_URL');
        $this->testMode = env('UPAY_TEST_MODE');
        $this->returnURL = env('UPAY_RETURN_URL');
        $this->failURL = env('UPAY_FAIL_URL');
        $this->cancelURL = env('UPAY_CANCEL_URL');
        $this->notificationURL = env('UPAY_NOTIFICATION_URL');
        $this->verificationURL = env('UPAY_VERIFICATION_URL');
    }

    /**
     * Get return callback url.
     *
     * @return string url
     */
    public function getReturnURL()
    {
        return $this->returnURL;
    }

     /**
     * Get fail callback url.
     *
     * @return string url
     */
    public function getFailURL()
    {
        return $this->failURL;
    }

     /**
     * Get cancel callback url.
     *
     * @return string url
     */
    public function getCancelURL()
    {
        return $this->cancelURL;
    }

     /**
     * Get notification callback url.
     *
     * @return string url
     */
    public function getNotificationURL()
    {
        return $this->notificationURL;
    }

    /**
     * Get verification callback url.
     *
     * @return string url
     */
    public function getVerificationURL()
    {
        return $this->verificationURL;
    }

    /**
     * Sets the password for HTTP basic authentication.
     *
     * @param string $secret Password for HTTP basic authentication
     *
     * @return Configuration
     */
    public function setSecret(string $secret): self
	{
    	$this->secret = $secret;

    	return $this;
	}

    /**
     * Gets the password for HTTP basic authentication.
     *
     * @return string Password for HTTP basic authentication
     */
	public function getSecret()
	{
		return $this->secret;
	}

    /**
     * Sets the shopId for HTTP basic authentication.
     *
     * @param string $id Username for HTTP basic authentication
     *
     * @return Configuration
     */
    public function setShopId($id): self
    {
        $this->shopId = $id;

        return $this;
    }

    /**
     * Gets the shopId for HTTP basic authentication.
     *
     * @return string Username for HTTP basic authentication
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set the checkout host.
     *
     * @return string Host
     */
    public function setCheckoutHost($host)
    {
        return $this->hostCheckout = $host;
    }
    
    /**
     * Gets the checkout host.
     *
     * @return string Host
     */
    public function getCheckoutHost()
    {
        return $this->hostCheckout;
    }

    /**
     * Set the payment host.
     *
     * @return string Host
     */
    public function setPaymentHost($host)
    {
        return $this->hostPayment = $host;
    }
    
    /**
     * Gets the payment host.
     *
     * @return string Host
     */
    public function getPaymentHost()
    {
        return $this->hostPayment;
    }
    
    /**
     * Sets api public key.
     *
     * @param bool $test Debug flag
     *
     * @return Configuration
     */
    public function setPublicKey(string $key): self
    {
        $this->publicKey = $key;

        return $this;
    }

    /**
     * Sets test mode flag.
     *
     * @param bool $test Debug flag
     *
     * @return Configuration
     */
    public function setTestMode($test = false): self
    {
        $this->testMode = $test;

        return $this;
    }

    /**
     * Get api public key.
     *
     * @param bool $test Debug flag
     *
     * @return Configuration
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Gets the test mode flag.
     *
     * @return bool
     */
    public function getTestMode()
    {
        return $this->testMode;
    }
}
