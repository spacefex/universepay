<?php

namespace UniversePay;

use UniversePay\Language;
use UniversePay\Money;
use UniversePay\Configuration;
use Illuminate\Support\Facades\Log;

class GetPaymentTokenData
{
    public $customer;
    public $money;
    public $apiConfig;
    protected $description;
    protected $tracking_id;
    protected $success_url;
    protected $decline_url;
    protected $fail_url;
    protected $cancel_url;
    protected $notification_url;
    protected $transaction_type;
    protected $payment_methods;
    protected $expired_at;
    protected $test_mode;
    protected $attempts;

    public function __construct()
    {
        $this->customer = new Customer();
        $this->money = new Money();
        $this->setPaymentTransactionType();
        $this->language = Language::getDefaultLanguage();
        $this->expired_at = NULL;
        $this->payment_methods = [];
        $this->test_mode = false;
        $this->attempts = NULL;
        $this->apiConfig = new Configuration();
    }

    public function buildRequestData()
    {
        $request = [
            'checkout' => [
                'transaction_type' => $this->getTransactionType(),
                'attempts' => $this->getAttempts(),
                'test' => $this->getTestMode(),
                'version' => 2,
                'order' => [
                    'amount' => $this->money->getCents(),
                    'currency' => $this->money->getCurrency(),
                    'description' => $this->getDescription(),
                    'tracking_id' => $this->getTrackingId(),
                    'expired_at' => $this->getExpiryDate()
                ],
                'settings' => [
                    'return_url' => $this->apiConfig->getReturnURL(),
                    'fail_url' => $this->apiConfig->getFailURL(),
                    'cancel_url' => $this->apiConfig->getCancelURL(),
                    'notification_url' => $this->apiConfig->getNotificationURL(),
                    // 'verification_url' => $this->apiConfig->getVerificationURL(),
                    'language' => $this->getLanguage(),
                    // 'customer_fields' => [
                    //     'read_only' => $this->getReadonlyFields(),
                    //     'visible' => $this->getVisibleFields()
                    // ]
                ],
                'customer' => [
                    'email' => $this->customer->getEmail(),
                    'first_name' => $this->customer->getFirstName(),
                    'last_name' => $this->customer->getLastName(),
                    'country' => $this->customer->getCountry(),
                    'city' => $this->customer->getCity(),
                    'state' => $this->customer->getState(),
                    'zip' => $this->customer->getZip(),
                    'address' => $this->customer->getAddress(),
                    'phone' => $this->customer->getPhone(),
                    'birth_date' => $this->customer->getBirthDate()
                ]
            ]
        ];

        if (is_null($this->getAttempts())) {
            unset($request['checkout']['attempts']);
        }

        return $request;
    }

    public function setDescription($description)
    {
        if (strlen($description) == 0 || $description == null) {
            $this->description = 'UniversalPay transaction default desc';
        }
        else {
            $this->description = $description;
        }
    }
    public function getDescription()
    {
        return $this->description;
    }

    public function setTrackingId($tracking_id)
    {
        $this->tracking_id = $tracking_id;
    }
    public function getTrackingId()
    {
        return $this->tracking_id;
    }

    public function setAuthorizationTransactionType()
    {
        $this->transaction_type = 'authorization';
    }

    public function setPaymentTransactionType()
    {
        $this->transaction_type = 'payment';
    }

    public function setTokenizationTransactionType()
    {
        $this->transaction_type = 'tokenization';
    }

    public function getTransactionType()
    {
        return $this->transaction_type;
    }

    public function setLanguage($language_code = false)
    {
        if ($language_code === false) {
            $language_code = \app()->getLocale();
            
            // remove not ntcessary -GB from locale (ex.: en-GB)
            $language_code = substr($language_code, 0, 2);
        }

        if (in_array($language_code, Language::getSupportedLanguages())) {
            $this->language = $language_code;
        } else {
            $this->language = Language::getDefaultLanguage();
        }
    }

    public function getLanguage()
    {
        return $this->language;
    }

    # date when payment expires for payment
    # date is in ISO8601 format
    public function setExpiryDate($date)
    {
        $iso8601 = NULL;

        if ($date != NULL)
            $iso8601 = date(DATE_ISO8601, strtotime($date));

        $this->expired_at = $iso8601;
    }

    public function getExpiryDate()
    {
        return $this->expired_at;
    }

    public function setTestMode($mode = true)
    {
        $this->test_mode = $mode;
    }

    public function getTestMode()
    {
        return $this->test_mode;
    }

    public function setAttempts($attempts = 3)
    {
        $this->attempts = $attempts;
    }

    public function getAttempts()
    {
        return $this->attempts;
    }
}