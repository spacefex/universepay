<?php

namespace UniversePay;

class Customer
{
    protected $ip;
    protected $email;

    protected $first_name;
    protected $last_name;
    protected $address;
    protected $city;
    protected $country;
    protected $state;
    protected $zip;
    protected $phone;
    protected $birth_date = NULL;

    public function setIP($ip)
    {
        $this->ip = $this->setNullIfEmpty($ip);
    }
    public function getIP()
    {
        return $this->ip;
    }

    public function setEmail($email)
    {
        $this->email = $this->setNullIfEmpty($email);
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setFirstName($first_name)
    {
        $this->first_name = $this->setNullIfEmpty($first_name);
    }
    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $this->setNullIfEmpty($last_name);
    }
    public function getLastName()
    {
        return $this->last_name;
    }

    public function setAddress($address)
    {
        $this->address = $this->setNullIfEmpty($address);
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setCity($city)
    {
        $this->city = $this->setNullIfEmpty($city);
    }
    public function getCity()
    {
        return $this->city;
    }

    public function setCountry($country)
    {
        $this->country = $this->setNullIfEmpty($country);
    }
    public function getCountry()
    {
        return $this->country;
    }

    public function setState($state)
    {
        $this->state = $this->setNullIfEmpty($state);
    }
    public function getState()
    {
        return (in_array($this->country, ['US', 'CA'])) ? $this->state : null;
    }

    public function setZip($zip)
    {
        $this->zip = $this->setNullIfEmpty($zip);
    }
    public function getZip()
    {
        return $this->zip;
    }

    public function setPhone($phone)
    {
        $this->phone = $this->setNullIfEmpty($phone);
    }
    public function getPhone()
    {
        return $this->phone;
    }

    public function setBirthDate($birthdate)
    {
        $this->birth_date = $this->setNullIfEmpty($birthdate);
    }
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    private function setNullIfEmpty(&$resource)
    {
        return (strlen($resource) > 0) ? $resource : null;
    }
}
