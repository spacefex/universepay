<?php

return [
	'shop_id' => env('UPAY_SHOP_ID'),
	'secret' => env('UPAY_SECRET'),
	'api_url' => env('UPAY_URL'),
	'test_mode' => env("UPAY_TEST_MODE", false),
	'return_url' => env("UPAY_RETURN_URL"),
	'success_url' => env("UPAY_SUCCESS_URL"),
	'decline_url' => env("UPAY_DECLINE_URL"),
	'fail_url' => env("UPAY_FAIL_URL"),
	'cancel_url' => env("UPAY_CANCEL_URL"),
	'notification_url' => env("UPAY_NOTIFICATION_URL"),
	'verification_url' => env("UPAY_VERIFICATION_URL"),
];
