<?php

namespace UniversePay;

use Illuminate\Support\ServiceProvider;

/**
 * Class SumSubServiceProvider
 * @package SumSub
 */
class UniversePayServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Determine if this is a Lumen application.
     *
     * @return bool
     */
    protected function isLumen(): bool
    {
        return str_contains($this->app->version(), 'Lumen');
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if (!$this->isLumen()) {
            $this->publishes([
                __DIR__.'/Config/universepay.php' => config_path('universepay.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(UniversePayApi::class, function () {
            $api = new UniversePayApi();

            return $api;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [UniversePayApi::class];
    }

}
