<?php

namespace UniversePay;

use Illuminate\Support\Facades\Log;
use Illuminate\HTTP\Request;

/**
 * Class UniversePay
 * @package UniversePay
 */
class UniversePayApi
{
	/**
	 * @var UniversePay;
	 */
	private $client;

	/**
	 * @var Config for UniversePay;
	 */
	private $config;

	function __construct()
	{
		$this->config = new Configuration();
	}

	public function checkDigest(Request $request)
	{
		$httpHeaderKeys = getallheaders();
        if (!isset($httpHeaderKeys['Content-Signature'])) {
            Log::info("[UPAY] Could not found Content-Signature in headers.");
            return false;
        }

		$signature = $httpHeaderKeys['Content-Signature'];
		$rawBody = $request->getContent();
		$shopPublicKey = $this->config->getPublicKey();
		$publicKey = str_replace(array("\r\n", "\n"), '', $shopPublicKey);
		$publicKey = chunk_split($publicKey, 64);
		$publicKey = "-----BEGIN PUBLIC KEY-----\n$publicKey-----END PUBLIC KEY-----";

		$signature = base64_decode($signature);
		$key = openssl_pkey_get_public($publicKey);
		$verifyResult = openssl_verify($rawBody, $signature, $key, OPENSSL_ALGO_SHA256);

		if ($verifyResult === 1) {
			return true;
		}

		Log::error("[UPAY] Digital signature verification failed.");

		return false;
	}

	/**
	 * @return UniversePayClient
	 * @throws ConfigException
	 */
	private function createClient()
	{
		if (!$this->config->getSecret()) {
			throw new \Exception('Please set your UniversePay API key in the config file');
		}

		return new UniversePayClient($this->config);
	}

	/**
	 * @return UniversePayClient
	 * @throws ConfigException
	 */
	public function getClient(): UniversePayClient
	{
		if (!$this->client) {
			$this->client = $this->createClient();
		}

		return $this->client;
	}

	public function getIPAddress() {
        //whether ip is from the share internet  
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
                $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }  
    
        //whether ip is from the proxy  
        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }  
        //whether ip is from the remote address  
        else {  
             $ip = $_SERVER['REMOTE_ADDR'];  
        }

        return $ip;  
    }  
}